terraform {
  required_providers {
    vkcs = {
      source  = "vk-cs/vkcs"
      version = "~> 0.5.2"
    }
  }
}

provider "vkcs" {
  # Your user account.
  username = "crazyflash@mail.ru"

  # The password of the account
  password = "Aa5141048"

  # The tenant token can be taken from the project Settings tab - > API keys.
  # Project ID will be our token.
  project_id = "3263ed6506704d71bd114d88e5d11843"

  # Region name
  region = "RegionOne"

  auth_url = "https://infra.mail.ru:35357/v3/"
}

variable "image_flavor" {
  type    = string
  default = "Ubuntu-22.04-202208"
}

variable "compute_flavor" {
  type    = string
  default = "STD2-1-2"
}

variable "key_pair_name" {
  type    = string
  default = "local-win"
}

variable "availability_zone_name" {
  type    = string
  default = "GZ1"
}

#NETWORK

data "vkcs_networking_network" "extnet" {
  name = "internet"
}

#MAIN.TF

data "vkcs_compute_flavor" "compute" {
  name = var.compute_flavor
}

data "vkcs_images_image" "compute" {
  name = var.image_flavor
}

resource "vkcs_compute_instance" "compute" {
  name              = "6_5"
  flavor_id         = data.vkcs_compute_flavor.compute.id
  key_pair          = var.key_pair_name
  security_groups   = ["my_group"]
  availability_zone = var.availability_zone_name

  block_device {
    uuid                  = data.vkcs_images_image.compute.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-hdd"
    volume_size           = 18
    boot_index            = 0
    delete_on_termination = true
  }

  config_drive = true

  network {
    name = "internet"
  }

  #user_data = "${file("${path.module}/user_data.sh")}"

  #depends_on = [
    #vkcs_networking_network.network
  #]
}

output "external_ip_only" {
  value = vkcs_compute_instance.compute.network[*].fixed_ip_v4
}

output "external_mac_only" {
  value = vkcs_compute_instance.compute.network[*].mac
}
